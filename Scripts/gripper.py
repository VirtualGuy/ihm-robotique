#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Box2D
from Box2D import (b2Vec2, b2RayCastCallback, b2RayCastInput, b2RayCastOutput, b2_pi)
import math
import numpy as np
from vehicle import Vehicle


class Gripper:

	_ax = 0.5
	_l = 4.8
	_ls = 3
	_w = 2
	_ws = 0.5
	_s = 0.01
	claw_vertices = [	[(-_ax,0), (-_w,_ws), (-_w,_l-_ls), (-_ax, _l), (-_w-_s,_l-_ls), (-_w-_s,_ws), (-_ax,0)],
						[(+_ax,0), (+_w,_ws), (+_w,_l-_ls), (+_ax, _l), (+_w-_s,_l-_ls), (+_w-_s,_ws), (+_ax,0)] ]


	def __init__(self, vehicle, position=(0,8.5), density=0.01, friction=0.001):
		self.vehicle = vehicle
		self.position = b2Vec2(vehicle.b2body.position) + position
		self.b2bodies = []
		self.b2joints = []
		
		
		
		for vertices in self.claw_vertices:
			b = vehicle.world.b2world.CreateDynamicBody(position=self.position)
			b.CreatePolygonFixture(vertices=vertices, density=density, friction=friction)
			j = vehicle.world.b2world.CreateRevoluteJoint( bodyA=vehicle.b2body,
															bodyB=b,
															localAnchorA=(b2Vec2(position) + vertices[0]),
															# center of tire
															localAnchorB=(0, 0),
															enableMotor=True,
															maxMotorTorque=1000,
															enableLimit=True,
															lowerAngle=-b2_pi/4,
															upperAngle=+b2_pi/4,
															collideConnected = True
															)
			j.frequencyHz = 0.0
			j.dampingRatio = 0.0

			self.b2bodies.append(b)
			self.b2joints.append(j)
			

	def step(self, fw):
		pass

		
	def draw(self, fw):
		for i, vertices in enumerate(self.claw_vertices):
			transform = self.b2bodies[i].transform
			vertices = [transform * v for v in vertices]
			fw.DrawPolygon( vertices, (255, 255, 255, 255))
			
	def open(self):
		self.b2joints[0].motorSpeed = +0.5
		self.b2joints[1].motorSpeed = -0.5

		
	def close(self):
		self.b2joints[0].motorSpeed = -0.5
		self.b2joints[1].motorSpeed = +0.5


	
	

		
