#!/usr/bin/env python
# -*- coding: utf-8 -*-

######## Robotic simulation project.
######## Self-driving in labyrinth.
######## Chloé Bryche, Maëlys Brelier and Simon Besga.

from IO import Framework, Keys
from world import World, Wall, Ball, Maze
from platforms import *
import numpy as np
import pandas as pd

class MyRover( Rover):
	######## Variable qui gère la vitesse du véhicule.
	### /!\/!\/!\ si mySpeed >= 9, 
	### /!\/!\/!\ le véhicule va trop vite pour faire tous les calculs 
	### /!\/!\/!\ et ne finit pas le labyrinthe
	mySpeed = 8

	######## Booléens utilisés pour maintenir un sens de rotation lors du demi-tour
	### Une fois la décision prise de faire le demitour dans un sens,
	### elles sont réinitialisé en sortie de demi-tour
	isTurningBackLeft = False
	isTurningBackRight = False

	######## Booléens utilisés pour bloqués l'avancement pendant 
	######## la phase de récuparation de la balle
	ballCatchingProcedure = False
	ballCatched = False

	######## Init pour l'instanciation
	def __init__(self, world, **vehicle_kws):
		super().__init__(world, **vehicle_kws)


	######## Boucle STEP mise à jour toutes les frames
	def step(self, fw):
		super().step(fw)
		
		########
		######## Perception
		########
		######## On analyse le laser pour garder la moyenne des valeurs à gauche, droite et centre.
		laser_data = np.array(self.laserscan.values)
		
		right  = laser_data[  :15].mean()
		center = laser_data[15:30].mean()
		left   = laser_data[30:  ].mean()
		
		fw.DrawText('Left: ' + str(int(left*10)/10.0)
					+ ', Center: ' + str(int(center*10)/10.0)
					+ ', Right: ' + str(int(right*10)/10.0)
					+ ', Ball_Catching_Procedure: ' + str(self.ballCatchingProcedure)
					+ ', Ball_Catched: ' + str(self.ballCatched)
					+ ', Robot position: ' + str(self.b2body.position))
		
		########
		######## Decision
		########
		######## motor_control = [ vitesse linéaire (positif est vers l'avant),
		########					vitesse angulaire (positif est anti-horaire) ]

		### On vérifie qu'on est pas en train d'essayer d'attraper la balle
		if not(self.ballCatchingProcedure):
			### ALGORITHME DE DEPLACEMENT - MAIN GAUCHE
			### Choix de l'action à réaliser ( = paramétrage de motor_control) en fonction du cas
			
			### Cas où on va tout droit
			if (left < center  and left < right) or (right < center and left < center):
				motor_control = (+self.mySpeed, 0)
				# print('TOUT DROIT ' + str(motor_control)) ## <------ uncomment to see log
			
			### Cas où on va à droite :
			if (center < right and left < right):
				motor_control = (+self.mySpeed, -self.mySpeed)
				# print('DROITE ' + str(motor_control)) ## <------ uncomment to see log

			### Cas où on va à gauche :
			if (center < right  and center < left) or (center < left and right < left and right <25 and center <25):
				if center < 28:
					motor_control = (+self.mySpeed, +self.mySpeed)
					# print('GAUCHE ' + str(motor_control)) ## <------ uncomment to see log

			### Cas d'urgence si on s'éloigne du mur Gauche pour s'en rapprocher :
			if left > 25  and not(right < center and left < center):
				motor_control = (+self.mySpeed, +self.mySpeed)

				## Ici on réinitialise les booléens car on passe par la après le demi-tour
				self.isTurningBackLeft = False
				self.isTurningBackRight = False
				# print('### URGENCE ELOIGNEMENT ' + str(motor_control)) ## <------ uncomment to see log

			### Cas où on s'arrete et on tourne sur soi-même - Demi-tour :
			if right < 25 and left <25 and center <20:
				if left < right and not(self.isTurningBackLeft):
					self.isTurningBackRight = True
					motor_control = (-self.mySpeed/3, -self.mySpeed)
				if right < left and not(self.isTurningBackRight):
					self.isTurningBackLeft = True
					motor_control = (-self.mySpeed/3, +self.mySpeed)
				# print('######### DEMI-TOUR ### DROITE ' + str(self.isTurningBackRight)  + '### GAUCHE ' + str(self.isTurningBackLeft) + str(motor_control))

			### Cas d'urgence si on se rapproche trop du mur gauche :
			if left < 10 :
				motor_control = (-self.mySpeed/2, -self.mySpeed/2)
				# print('###### URGENCE CHOC GAUCHE ' + str(motor_control)) ## <------ uncomment to see log

			### Cas d'urgence si on se rapproche trop du mur droit:
			if right < 10 :
				motor_control = (-self.mySpeed/2, +self.mySpeed/2)
				# print('###### URGENCE CHOC DROIT ' + str(motor_control)) ## <------ uncomment to see log


			### On verifie aussi les capteurs de proximités
			### Capteurs à l'avant
			if self.front_ir.values[0] < 6:
				motor_control = (motor_control[0],-self.mySpeed)
			if self.front_ir.values[2] < 6:
				motor_control = (motor_control[0],+self.mySpeed)
			### Capteurs à l'arriere
			if self.rear_ir.values[0] < 6:
				motor_control = (motor_control[0],+self.mySpeed)
			if self.rear_ir.values[2] < 6:
				motor_control = (motor_control[0],-self.mySpeed)
			if self.rear_ir.values[1] < 6:
				motor_control = (+self.mySpeed,0)


		### On vérifie la présence de la balle dans l'ensemble des rayons
		### La balle étant un objet dynamique, on vérifie le type des objets que l'on touche avec les lasers
		### type == 0 pour les dynamiques et type == 1 pour les statiques
		
		currentLaserBall = 0
		if not(self.ballCatched):
			for idx in range(len(self.laserscan.values)):
				if self.laserscan.array[idx] != None and self.laserscan.array[idx].hit == True:
					if self.laserscan.array[idx].fixture.type == 0:
						currentLaserBall = idx
						self.ballCatchingProcedure = True
						self.gripper.open()

		### Si on a repéré la balle dans un des lasers
		### On passe en procédure de Catching
		### Déplacement spécifique pour attrapper la balle
		### On se tourne en évitant les murs pour avoir la balle en face

		if self.ballCatchingProcedure:
			motor_control = (0,0)
			if currentLaserBall < 22 and right < 25:
				motor_control = (self.mySpeed/3, -self.mySpeed/3)			
			if 24 < currentLaserBall < 45 and left < 25:
				motor_control = (self.mySpeed/3, self.mySpeed/3)
			if (self.laserscan.array[22].hit != True and self.laserscan.array[22].fixture.type != 0 and self.laserscan.values[22] > 15) and (self.laserscan.array[23].hit != True and self.laserscan.array[23].fixture.type != 0 and self.laserscan.values[23] > 15):
				motor_control = (self.mySpeed/self.mySpeed,+self.mySpeed/2)
			else:
				motor_control = (self.mySpeed/3,0)

			### On utilise le capteur avant centre pour vérifier si la balle est dans le gripper
			### On ferme le gripper
			if self.front_ir.values[1] < 3.5:
				self.gripper.close()
				self.ballCatchingProcedure = False
				self.ballCatched = True


		########
		######## Action
		########
		### Controle des moteurs
		### On convertit les vitesses (linéaire et angulaire) vers les vitesses des deux roues
		v_l = motor_control[0] - motor_control[1]/2
		v_r = motor_control[0] + motor_control[1]/2
		self.tires[0].update_drive( v_l ) # roue gauche		
		self.tires[1].update_drive( v_r )	# roue droite

		### Une fois dans le carré d'arrivé, on supprime le robot.
		endline = [-45,-20,10,40]
		if endline[0] < self.b2body.position[0] < endline[1] and endline[2] < self.b2body.position[1] < endline[3] :
			if self.ballCatched:
				world.bodies.remove(world.bodies[2])
				print('Balle récupérée.')
			world.bodies.remove(self)
			print('Sortie du labyrinthe réussie.')


########
######## Main
########
if __name__ == "__main__":
	world = World()
	
	world.bodies.append( Maze(world) )
	world.bodies.append( MyRover( world, position=(+90,-50)) )
	world.bodies.append( Ball( world, position=(-30,-30)) )

	screen = Framework("Simulation robot labyrinthe - Chloé, Maëlys et Simon", world)
	screen.PPM = 3.5
	screen.run()
