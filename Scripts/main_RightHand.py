#!/usr/bin/env python
# -*- coding: utf-8 -*-

######## Robotic simulation project.
######## Self-driving in labyrinth.
######## Chloé Bryche, Maëlys Brelier and Simon Besga.

from IO import Framework, Keys
from world import World, Wall, Ball, Maze
from platforms import *
import numpy as np
import pandas as pd

class MyRover( Rover):
	######## Variable qui gère la vitesse du véhicule.
	### /!\/!\/!\ si mySpeed >= 9, 
	### /!\/!\/!\ le véhicule va trop vite pour faire tous les calculs 
	### /!\/!\/!\ et ne finit pas le labyrinthe
	mySpeed = 8

	######## Booléens utilisés pour maintenir un sens de rotation lors du demi-tour
	### Une fois la décision prise de faire le demitour dans un sens,
	### elles sont réinitialisé en sortie de demi-tour
	isTurningBackLeft = False
	isTurningBackRight = False

	######## Init pour l'instanciation
	def __init__(self, world, **vehicle_kws):
		super().__init__(world, **vehicle_kws)


	######## Boucle STEP mise à jour toutes les frames
	def step(self, fw):
		super().step(fw)
		
		########
		######## Perception
		########
		######## On analyse le laser pour garder la moyenne des valeurs à gauche, droite et centre.
		laser_data = np.array(self.laserscan.values)
		
		right  = laser_data[  :15].mean()
		center = laser_data[15:30].mean()
		left   = laser_data[30:  ].mean()
		
		fw.DrawText('left: ' + str(int(left*10)/10.0)
					+ ', center: ' + str(int(center*10)/10.0)
					+ ', right: ' + str(int(right*10)/10.0)
					+ ', position: ' + str(self.b2body.position))
		
		########
		######## Decision
		########
		######## motor_control = [ vitesse linéaire (positif est vers l'avant),
		########					vitesse angulaire (positif est anti-horaire) ]

		### ALGORITHME MAIN DROITE
		### Choix de l'action à réaliser ( = paramétrage de motor_control) en fonction du cas
		
		### Cas où on va tout droit
		if (right < center  and right < left) or (right < center and left < center):
			motor_control = (+self.mySpeed, 0)
			#print('TOUT DROIT ' + str(motor_control)) ## <------ uncomment to see log
		
		### Cas où on va à droite :
		if (center < right  and center < left) or (center < right and left < right):
			motor_control = (+self.mySpeed, -self.mySpeed)
			#print('DROITE ' + str(motor_control)) ## <------ uncomment to see log

		### Cas où on va à gauche :
		if center < left and right < left:
			if center < 28:
				motor_control = (+self.mySpeed, +self.mySpeed)
				#print('GAUCHE ' + str(motor_control)) ## <------ uncomment to see log

		### Cas d'urgence si on s'éloigne du mur Droit pour s'en rapprocher :
		if right > 25  and not(right < center and left < center):
			motor_control = (+self.mySpeed/2, -self.mySpeed)

			## Ici on réinitialise les booléens car on passe par la après le demi-tour
			self.isTurningBackLeft = False
			self.isTurningBackRight = False
			#print('### URGENCE ELOIGNEMENT ' + str(motor_control)) ## <------ uncomment to see log

		### Cas où on s'arrete et on tourne sur soi-même - Demi-tour :
		if right < 25 and left <25 and center <20:
			if left < right and not(self.isTurningBackLeft):
				self.isTurningBackRight = True
				motor_control = (-self.mySpeed/3, -self.mySpeed)
			if right < left and not(self.isTurningBackRight):
				self.isTurningBackLeft = True
				motor_control = (-self.mySpeed/3, +self.mySpeed)
			#print('######### DEMI-TOUR ### DROITE ' + str(self.isTurningBackRight)  + '### GAUCHE ' + str(self.isTurningBackLeft) + str(motor_control))

		### Cas d'urgence si on se rapproche trop d'un mur:
		if right < 10 :
			motor_control = (-self.mySpeed, +self.mySpeed)
			#print('###### URGENCE CHOC ' + str(motor_control)) ## <------ uncomment to see log

		### On verifie aussi les capteurs de proximités
		### Capteurs à l'avant
		if self.front_ir.values[0] < 6:
			motor_control = (motor_control[0],-self.mySpeed)
		if self.front_ir.values[2] < 6:
			motor_control = (motor_control[0],+self.mySpeed)
		### Capteurs à l'arriere
		if self.rear_ir.values[0] < 6:
			motor_control = (motor_control[0],+self.mySpeed)
		if self.rear_ir.values[2] < 6:
			motor_control = (motor_control[0],-self.mySpeed)
		if self.rear_ir.values[1] < 6:
			motor_control = (+self.mySpeed,0)
		
		########
		######## Action
		########
		### Controle des moteurs
		### On convertit les vitesses (linéaire et angulaire) vers les vitesses des deux roues
		v_l = motor_control[0] - motor_control[1]/2
		v_r = motor_control[0] + motor_control[1]/2
		self.tires[0].update_drive( v_l ) # roue gauche		
		self.tires[1].update_drive( v_r )	# roue droite

		### Une fois dans le carré d'arrivé, on supprime le robot.
		endline = [-45,-20,10,40]
		if endline[0] < self.b2body.position[0] < endline[1] and endline[2] < self.b2body.position[1] < endline[3] :
			world.bodies.remove(self)
			print('Sortie du labyrinthe réussie.')


########
######## Main
########
if __name__ == "__main__":
	world = World()
	
	world.bodies.append( Maze(world) )
	world.bodies.append( MyRover( world, position=(+90,-50)) )
	world.bodies.append( Ball( world, position=(-30,-30)) )

	screen = Framework("Simulation robot labyrinthe - Chloé, Maëlys et Simon", world)
	screen.PPM = 3.5
	screen.run()
